package de.hackathon.iotflowers.client;

import de.hackathon.iotflowers.Connector;
import de.hackathon.iotflowers.factory.ListenerRegistry;
import de.hackathon.iotflowers.packet.Packet;
import de.hackathon.iotflowers.thread.Callback;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.log4j.Log4j2;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Lennox on 18.03.17.
 */
@Log4j2
public class ConnectionHandler extends SimpleChannelInboundHandler<Packet> {

    private Connector instance = Connector.getInstance();

    private ListenerRegistry listenerRegistry;

    private ChannelHandlerContext ctx;

    private Set<Callback<Void>> connectedCallback = Collections.synchronizedSet(new HashSet<>());
    private Set<Callback<Void>> disconnectedCallback = Collections.synchronizedSet(new HashSet<>());
    private Set<Callback<Throwable>> exceptionCallback = Collections.synchronizedSet(new HashSet<>());

    public ConnectionHandler() {
        this.listenerRegistry = Connector.getListenerRegistry();
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        this.ctx = ctx;

        if (this.connectedCallback.size() > 0) {
            this.connectedCallback.forEach(callback -> callback.done(null));
            this.connectedCallback.clear();
        }
    }

    public boolean isChannelOpen() {
        Channel channel = this.ctx == null ? null : this.ctx.channel();
        return (channel != null && channel.isOpen());
    }

    public Channel getChannel() {
        return ctx.channel();
    }

    public void dispatchPacket(Packet packet) {
        if (!isChannelOpen()) return;
        Channel channel = getChannel();

        if (!packet.isAsync()) {
            // Sync sending
            channel.writeAndFlush(packet).addListener(ChannelFutureListener.FIRE_EXCEPTION_ON_FAILURE);
            return;
        }
        // Async sending
        channel.eventLoop().execute(() -> channel.writeAndFlush(packet).addListener(ChannelFutureListener.FIRE_EXCEPTION_ON_FAILURE));
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        this.ctx = null;

        this.instance.setLogin(false);

        if (this.disconnectedCallback.size() > 0) {
            this.disconnectedCallback.forEach(callback -> callback.done(null));
            this.disconnectedCallback.clear();
        }
    }

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, Packet packet) throws Exception {
        if (!this.ctx.channel().isOpen()) return;
        this.listenerRegistry.callEvent(channelHandlerContext, packet);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        if (ctx.channel().isActive()) {
            ctx.close();
        }

        if (this.exceptionCallback.size() > 0) {
            this.exceptionCallback.forEach(callback -> callback.done(cause));
        } else {
            throw new Exception(cause);
        }
    }

    public void whenConnected(Callback<Void> callback) {
        connectedCallback.add(callback);
    }

    public void onException(Callback<Throwable> callback) {
        exceptionCallback.add(callback);
    }

    public void whenDisconnected(Callback<Void> callback) {
        disconnectedCallback.add(callback);
    }
}
