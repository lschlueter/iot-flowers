package de.hackathon.iotflowers.client;

import de.hackathon.iotflowers.Connector;
import de.hackathon.iotflowers.factory.BootstrapFactory;
import de.hackathon.iotflowers.packet.PacketDecoder;
import de.hackathon.iotflowers.packet.PacketEncoder;
import de.hackathon.iotflowers.packet.PacketRegistry;
import de.hackathon.iotflowers.packet.packets.LoginRequestPacket;
import de.hackathon.iotflowers.thread.AsyncExecutor;
import de.hackathon.iotflowers.thread.Callback;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import static de.hackathon.iotflowers.factory.BootstrapFactory.*;

/**
 * Created by Lennox on 19.03.17.
 */
@Log4j2
public class Client {

    private String ip;
    private int port;

    public Client(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    @Getter
    @Setter
    private static boolean connected;

    @Getter
    private ConnectionHandler connectionHandler;

    public void startConnection() {
        AsyncExecutor.run(() -> {
            Bootstrap bootstrap = buildBootstrap(connectionHandler -> {
                connectionHandler.whenConnected(args -> {
                    this.connectionHandler = connectionHandler;
                    connected = true;

                    log.info("Connection established; login...");
                    Connector.getInstance().onConnect();

                    // Login
                    connectionHandler.dispatchPacket(new LoginRequestPacket(Connector.getInstance().getConnectionName(), Connector.getInstance().getClientType()));

                });
                connectionHandler.whenDisconnected(args -> {
                    this.connectionHandler = null;
                    connected = false;

                    log.fatal("Connection closed! Reconnecting in 5s...");
                    Connector.getInstance().onDisconnect();

                    scheduleConnect();
                });
                connectionHandler.onException(exception -> log.error("ConnectionHandler threw an Exception: " + exception.toString()));
            });
            try {
                ChannelFuture channelFuture = bootstrap.connect(this.ip, this.port).sync();
                channelFuture.channel().closeFuture().sync();
            } catch (Exception e) {
                connected = false;
                log.fatal("Could not connect (" + this.ip + ":" + this.port + ") cause: " + e.getMessage() + "; retrying in 5 sec.");

                // Reconnect
                scheduleConnect();
            }
        });
    }

    private Bootstrap buildBootstrap(Callback<ConnectionHandler> connectionHandlerCallback) {
        return new BootstrapFactory().setChannelChannelInitializer(new ChannelInitializer<Channel>() {

            @Override
            protected void initChannel(Channel channel) throws Exception {
                ConnectionHandler connectionHandler = new ConnectionHandler();
                PacketRegistry packetRegistry = Connector.getPacketRegistry();
                channel.pipeline().addLast(FRAME_DECODER, new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 4, 0, 4));
                channel.pipeline().addLast(PACKET_DECODER, new PacketDecoder(packetRegistry));
                channel.pipeline().addLast(FRAME_PREPENDER, new LengthFieldPrepender(4));
                channel.pipeline().addLast(PACKET_ENCODER, new PacketEncoder(packetRegistry));
                channel.pipeline().addLast(CONNECTION_HANDLER, connectionHandler);
                connectionHandlerCallback.done(connectionHandler);
            }
        }).buildBootstrap();
    }

    private void scheduleConnect() {
        new Timer().schedule(new TimerTask() {

            @Override
            public void run() {
                startConnection();
            }
        }, TimeUnit.SECONDS.toMillis(5));
    }
}
