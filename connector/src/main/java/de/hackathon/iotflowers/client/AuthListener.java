package de.hackathon.iotflowers.client;

import de.hackathon.iotflowers.Connector;
import de.hackathon.iotflowers.packet.PacketHandler;
import de.hackathon.iotflowers.packet.PacketListener;
import de.hackathon.iotflowers.packet.packets.LoginResultPacket;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.log4j.Log4j2;

/**
 * Created by Lennox on 29.11.16.
 */
@Log4j2
public class AuthListener extends PacketListener {

    private Connector instance = Connector.getInstance();

    @PacketHandler
    public void onAuthListener(ChannelHandlerContext channelHandlerContext, LoginResultPacket loginResultPacket) {
        if (loginResultPacket.isSuccess()) {
            instance.setLogin(true);
            log.info("Logged in successfully!");
            return;
        }
        log.fatal("Could not login! Shutting down..");
        Connector.getInstance().disable();
    }
}
