package de.hackathon.iotflowers;

import de.hackathon.iotflowers.client.AuthListener;
import de.hackathon.iotflowers.client.Client;
import de.hackathon.iotflowers.factory.ListenerRegistry;
import de.hackathon.iotflowers.packet.Packet;
import de.hackathon.iotflowers.packet.PacketRegistry;
import de.hackathon.iotflowers.packet.packets.LoginRequestPacket;
import de.hackathon.iotflowers.packet.packets.LoginResultPacket;
import de.hackathon.iotflowers.packet.packets.PlantActionPacket;
import de.hackathon.iotflowers.packet.packets.WrapperStatusPacket;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;


/**
 * Created by Lennox on 28.11.16.
 */
@Log4j2
@Data
public abstract class Connector {

    @Getter
    private static Connector instance;

    // ClientData
    private Client client;
    private ClientType clientType;
    private String connectionName, key;

    // Registry
    @Getter
    @Setter
    private static PacketRegistry packetRegistry;

    @Getter
    @Setter
    private static ListenerRegistry listenerRegistry;

    private boolean login;

    public Connector(String connectionName, ClientType clientType) {
        this.connectionName = connectionName;
        this.clientType = clientType;
        instance = this;
    }

    public void enable(String ip, int port) {

        // Create instance if no exits
        if (packetRegistry == null) packetRegistry = new PacketRegistry();
        if (listenerRegistry == null) listenerRegistry = new ListenerRegistry();

        // Register Packets/Listeners
        registerPackets();
        registerListeners();

        // Starts the Client
        (this.client = new Client(ip, port)).startConnection();
    }

    private void registerPackets() {
        packetRegistry.registerPacket(0, LoginRequestPacket.class);
        packetRegistry.registerPacket(1, LoginResultPacket.class);
        packetRegistry.registerPacket(2, WrapperStatusPacket.class);
        packetRegistry.registerPacket(3, WrapperStatusPacket.class);
        packetRegistry.registerPacket(4, PlantActionPacket.class);
    }

    private void registerListeners() {
        listenerRegistry.register(new AuthListener());
    }

    public void sendPacket(Packet packet) {
        if (!Client.isConnected()) {
            log.error("Client has no connection...");
            return;
        }
        this.client.getConnectionHandler().dispatchPacket(packet);
    }

    public void disable() {
        this.client.getConnectionHandler().getChannel().disconnect().syncUninterruptibly();
    }

    public abstract void onConnect();

    public abstract void onDisconnect();
}