package de.hackathon.iotflowers.iotpflanze;

import java.util.ArrayList;

public class DataCache {
    private static DataCache ourInstance;

    private ArrayList<PlantStatus> data;
    private String username;

    public static DataCache getInstance() {
        if (ourInstance == null) {
            ourInstance = new DataCache();
        }
        return ourInstance;
    }

    private DataCache() {
        this.data = new ArrayList<>();
        testData();
    }

    public PlantStatus get(String name) {
        int id = 0;
        for (int i = 0; i < this.data.size(); i++) {
            if (this.data.get(i).getPlantName().equalsIgnoreCase(name)) {
                id = i;
            }
        }
        return this.data.get(id);
    }

    public ArrayList<PlantStatus> getData() {
        return this.data;
    }

    public String getUsername() {
        return this.username;
    }

    public void delete(PlantStatus plantStatus){

        int id = -1;
        for (int i = 0; i < this.data.size(); i++) {
            PlantStatus status = this.data.get(i);
            if (status.getPlantName().equals(plantStatus.getPlantName())) {
                id = i;
                break;
            }
        }
        if (id != -1) {
            this.data.remove(id);
        }

        // TODO: send delete Packet
    }

    public void create(PlantStatus plantStatus) {

        this.data.add(plantStatus);

        // TODO: send Create Packet
    }


    //TODO:
    private void testData(){
        data.add(new PlantStatus("Kaktus", 183, "N05-018"));
        data.add(new PlantStatus("Rose", 269, "N05-018"));
        data.add(new PlantStatus("Tulpe", 12, "W02-012"));
        data.add(new PlantStatus("Palme", 100, "S00-014"));
        data.add(new PlantStatus("Erdbeere", 300, "O02-088"));

    }
/*
    public void insert(Plant plant) {
        for (PlantStatus status : this.data) {
            if (status.getPlantName().equalsIgnoreCase(plant.getName())) {

            }
        }
    }*/
}
