package de.hackathon.iotflowers.iotpflanze;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.ByteArrayOutputStream;

public class PlantStatus implements Parcelable {

    private static int MAX_SENSOR_VALUE = 300;
    private String plantName;
    private int plantFill;
    private String roomName, imagePath;
    private Drawable image;

    public PlantStatus(String plantName, int plantFill, String roomName) {
        this.plantName = plantName;
        this.plantFill = plantFill;
        this.roomName = roomName;
    }

    public PlantStatus(String plantName, int plantFill, String roomName, String imagePath) {
        this.plantName = plantName;
        this.plantFill = plantFill;
        this.roomName = roomName;
        this.imagePath = imagePath;
    }

    public PlantStatus(String plantName, int plantFill, String roomName, byte[] image) {
        this.plantName = plantName;
        this.plantFill = plantFill;
        this.roomName = roomName;
        //this.image = decodeImage(image);
    }

    private Bitmap decodeImage(byte[] image) {

        /*ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byteArray = stream.toByteArray();
        bitmap.recycle();*/

        return null;
    }

    private byte[] encodeImage(Bitmap bitmap) {
        return null;
    }

    public String getPlantName() {
        return plantName;
    }

    public int getPlantFill() {
        return plantFill;
    }

    public int getPlantFillPercent() {
        // double test = plantFill/MAX_SENSOR_VALUE;
        return this.getPlantFill()/3;
    }

    public Drawable getImage() {
        return image;
    }

    /*public Bitmap getBitmap() {
         if (this.image == null) {
            this.image = new byte[] { 0 };
        }
        return BitmapFactory.decodeByteArray(this.image , 0, this.image.length);
        return null;
    }*/

    public String getPlantFillPercentText() {
        return this.getPlantFillPercent() + " %";
    }

    public String getRoomName() {
        return roomName;
    }

    private PlantStatus(Parcel in) {
        plantName = in.readString();
        plantFill = in.readInt();
        roomName = in.readString();
        imagePath = in.readString();
    }

    public String getImagePath() {
        return this.imagePath;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(this.getPlantName());
        out.writeInt(this.getPlantFill());
        out.writeString(this.getRoomName());
        out.writeString(this.getImagePath());
    }

    public int describeContents() {
        return this.hashCode();
    }

    public static final Parcelable.Creator<PlantStatus> CREATOR =
            new Parcelable.Creator<PlantStatus>() {
                public PlantStatus createFromParcel(Parcel in) {
                    return new PlantStatus(in);
                }

                public PlantStatus[] newArray(int size) {
                    return new PlantStatus[size];
                }
            };
}
