package de.hackathon.iotflowers.iotpflanze;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;


public class SinglePlantActivity extends AppCompatActivity {

    private PlantStatus plantStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_single_plant);

        this.plantStatus = (PlantStatus) this.getIntent().getExtras().get("plant");

        findViewById(R.id.deleteButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DataCache.getInstance().delete(getPlantStatus());
                finish();
            }
        });

        fillData();
    }

    private void fillData() {
        TextView plantName = findViewById(R.id.plantName);
        plantName.setText(this.getPlantStatus().getPlantName());

        ProgressBar waterLevel = findViewById(R.id.waterLevel);
        waterLevel.setMax(100);
        waterLevel.setProgress(this.getPlantStatus().getPlantFillPercent());

        TextView waterLevelPercent = findViewById(R.id.waterLevelPercent);
        waterLevelPercent.setText(this.getPlantStatus().getPlantFillPercentText());

        TextView roomName = findViewById(R.id.roomName);
        roomName.setText(this.getPlantStatus().getRoomName());

        try {
            System.out.println("+++++++++++ " + this.getPlantStatus().getImagePath());
            File f=new File(this.getPlantStatus().getImagePath(), "profile.jpg");
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            ImageView img = findViewById(R.id.singlePlantImage);
            img.setImageBitmap(b);
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }

        //image.setImageDrawable(getApplication().getResources().getDrawable(R.drawable.ic_launcher_foreground));


    }

    public PlantStatus getPlantStatus() {
        return plantStatus;
    }
}
