package de.hackathon.iotflowers.iotpflanze;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;

public class PlantListActivity extends AppCompatActivity {

    private ListView plantListView;
    private ArrayAdapterPlantStatus adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plant_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        try {
            plantListView = findViewById(R.id.plantListView);
            adapter = new ArrayAdapterPlantStatus(this, DataCache.getInstance().getData());
            plantListView.setAdapter(adapter);

            plantListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    PlantStatus plantStatus = DataCache.getInstance().getData().get(position);
                    Intent intent = new Intent(PlantListActivity.this.getApplication(), SinglePlantActivity.class);
                    intent.putExtra("plant", plantStatus);
                    startActivity(intent);
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PlantListActivity.this.getApplication(), NewPlantActivity.class);
                startActivity(intent);
            }
        });
        checkUpdates();
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            plantListView = findViewById(R.id.plantListView);
            adapter = new ArrayAdapterPlantStatus(this, DataCache.getInstance().getData());
            plantListView.setAdapter(adapter);

            plantListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    PlantStatus plantStatus = DataCache.getInstance().getData().get(position);
                    Intent intent = new Intent(PlantListActivity.this.getApplication(), SinglePlantActivity.class);
                    intent.putExtra("plant", plantStatus);
                    startActivity(intent);
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

        // checkUpdates();

    }

    public void checkUpdates() {
        for (PlantStatus plantStatus : DataCache.getInstance().getData()) {
            if (plantStatus.getPlantFillPercent() < 10) {
                notification(plantStatus);
            }
        }
    }

    public void notification(PlantStatus plantStatus) {
        // Create an explicit intent for an Activity in your app
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, "")
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle("Dein Blume ist durstig")
                .setContentText("Dein Wassertank von " + plantStatus.getPlantName() + " im Raum "
                        + plantStatus.getRoomName() + " muss aufgefüllt werden!")
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setVibrate(new long[] { 300, 300, 300, 300 })
                // Set the intent that will fire when the user taps the notification
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

        // notificationId is a unique int for each notification that you must define
        notificationManager.notify(98988, mBuilder.build());
    }

    public static ArrayList<PlantStatus> jsonToPlantList(JsonArray jsonArray){
        ArrayList<PlantStatus> data = new ArrayList<>();

        for (JsonElement jsonElement : jsonArray) {
            JsonObject tempObject = jsonElement.getAsJsonObject();
            PlantStatus tempStatus = new PlantStatus(
                    tempObject.get("name").getAsString(),
                    tempObject.get("fill").getAsInt(),
                    tempObject.get("room").getAsString());
            data.add(tempStatus);
        }

        return data;
    }

}
