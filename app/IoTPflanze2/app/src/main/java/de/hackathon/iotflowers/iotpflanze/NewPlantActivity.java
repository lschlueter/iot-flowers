package de.hackathon.iotflowers.iotpflanze;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.provider.MediaStore;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


public class NewPlantActivity extends AppCompatActivity {

    //private Drawable drawable;
    private String plantName, roomName, raspiID, imageName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_plant);

        ImageButton imageButton = findViewById(R.id.takeImageButton);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, 0);
            }
        });

        Button finishButton = findViewById(R.id.finishButton);
        finishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createNewPlant();
            }
        });
    }

    private void createNewPlant() {
        boolean cancel = false;

        this.readData();

        if (this.plantName.equals(null)) {
            cancel = true;
        }

        if (this.roomName.equals(null)) {
            cancel = true;
        }

        if (this.raspiID.equals(null)) {
            cancel = true;
        }

        if (cancel) {

        }
        else {
            DataCache.getInstance().create(new PlantStatus(this.plantName, 300, this.roomName, this.imageName));


            //TODO: create new Plant

            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap bitmap = (Bitmap) data.getExtras().get("data");

        ((ImageView)findViewById(R.id.testImage)).setImageBitmap(bitmap);




        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
            // Create imageDir
        File mypath=new File(directory,"profile.jpg");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
                // Use the compress method on the BitMap object to write image to the OutputStream
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        this.imageName = directory.getAbsolutePath();


    }

    public void readData() {
        plantName = ((EditText) findViewById(R.id.plantNameEditText)).getText().toString();
        roomName = ((EditText) findViewById(R.id.roomNameEditText)).getText().toString();
        raspiID = ((EditText) findViewById(R.id.uniqueIDEditText)).getText().toString();
    }
}
