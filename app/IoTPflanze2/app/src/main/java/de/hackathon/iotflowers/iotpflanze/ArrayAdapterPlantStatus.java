package de.hackathon.iotflowers.iotpflanze;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;


/**
 * Test
 */
public class ArrayAdapterPlantStatus extends ArrayAdapter<PlantStatus>{

    private Context context;

    public ArrayAdapterPlantStatus(Context context, ArrayList<PlantStatus> plantList){
        super(context, R.layout.plant_list_elements, plantList);
        this.context = context;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {

        PlantStatus plantStatus = getItem(position);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.plant_list_elements, parent, false);

        TextView plantName = row.findViewById(R.id.plantName);
        TextView plantFillText = row.findViewById(R.id.fillpercent);
        ProgressBar plantFill = row.findViewById(R.id.fillBar);
        plantFill.setMax(100);
        TextView roomName = row.findViewById(R.id.roomName);


        if (plantStatus != null) {
            plantName.setText(plantStatus.getPlantName());
            plantFillText.setText(plantStatus.getPlantFillPercentText());

            plantFill.setProgress(plantStatus.getPlantFillPercent());
            roomName.setText(plantStatus.getRoomName());
        }

        return row;
    }

}
