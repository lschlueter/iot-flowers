package de.hackathon.iotflowers;

import de.hackathon.iotflowers.sensor.Arduino;
import de.hackathon.iotflowers.thread.ShutdownThread;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.UUID;

@Log4j2
public class Wrapper {

    @Getter
    private static Connector connector;

    @Getter
    private static Arduino arduino;

    public static void main(String[] args) {
        System.setProperty("gnu.io.rxtx.SerialPorts", "/dev/tty.usbArduino");
        log.info("Initialising Wrapper..");

        // get the hostname
        log.info("Please enter the ip:");
        Scanner scanner = new Scanner(System.in);
        String ip = scanner.nextLine();
        scanner.close();

        // check uuid file
        String uuid = UUID.randomUUID().toString();
        Path path = Paths.get("wrapper.uuid");
        if (Files.exists(path)) {
            try {
                uuid = Files.readAllLines(path).get(0);
                log.info("Your uuid: " + uuid);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                try (PrintStream out = new PrintStream(new FileOutputStream(path.toFile()))) {
                    out.print(uuid);
                    out.close();
                }
                log.info("Your new uuid: " + uuid);
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
        }

        connector = new Connector(uuid, ClientType.FLOWER) {
            @Override
            public void onConnect() {
                arduino = new Arduino();
                arduino.initialize();
            }

            @Override
            public void onDisconnect() {
                if (arduino != null) arduino.close();
            }
        };
        connector.enable(ip, 3131);
        Runtime.getRuntime().addShutdownHook(new ShutdownThread());
    }
}
