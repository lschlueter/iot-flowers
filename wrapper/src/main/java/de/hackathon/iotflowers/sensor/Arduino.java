package de.hackathon.iotflowers.sensor;

import de.hackathon.iotflowers.Connector;
import de.hackathon.iotflowers.Wrapper;
import de.hackathon.iotflowers.packet.packets.WrapperStatusPacket;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import lombok.extern.log4j.Log4j2;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Enumeration;

@Log4j2
public class Arduino implements SerialPortEventListener {

    SerialPort serialPort;

    private static final String PORT_NAMES[] = {
            "/dev/ttyACM0", // Raspberry Pi
    };

    private BufferedReader input;
    private OutputStream output;
    private static final int TIME_OUT = 2000;
    private static final int DATA_RATE = 9600;

    public void initialize() {
        System.setProperty("gnu.io.rxtx.SerialPorts", "/dev/ttyACM0");

        CommPortIdentifier portId = null;
        Enumeration portEnum = CommPortIdentifier.getPortIdentifiers();

        while (portEnum.hasMoreElements()) {
            CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();
            for (String portName : PORT_NAMES) {
                if (currPortId.getName().equals(portName)) {
                    portId = currPortId;
                    break;
                }
            }
        }
        if (portId == null) {
            log.error("Could not find COM port.");
            return;
        }
        try {
            serialPort = (SerialPort) portId.open(this.getClass().getName(), TIME_OUT);
            serialPort.setSerialPortParams(DATA_RATE,
                    SerialPort.DATABITS_8,
                    SerialPort.STOPBITS_1,
                    SerialPort.PARITY_NONE);

            input = new BufferedReader(new InputStreamReader(serialPort.getInputStream()));
            output = serialPort.getOutputStream();

            serialPort.addEventListener(this);
            serialPort.notifyOnDataAvailable(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void close() {
        if (serialPort != null) {
            serialPort.removeEventListener();
            serialPort.close();
        }
    }

    public synchronized void serialEvent(SerialPortEvent oEvent) {
        if (oEvent.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
            try {
                String inputLine = input.readLine();

                // convert { wetLevel ; waterLevel }
                String[] input = inputLine.split(";");

                Connector connector = Wrapper.getConnector();
                if (connector == null) return;
                WrapperStatusPacket wrapperStatusPacket = new WrapperStatusPacket(
                        connector.getConnectionName(),
                        Integer.parseInt(input[1]),
                        Integer.parseInt(input[0]),
                        true
                );
                connector.sendPacket(wrapperStatusPacket);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}