package de.hackathon.iotflowers.thread;

import de.hackathon.iotflowers.Connector;
import de.hackathon.iotflowers.Wrapper;
import de.hackathon.iotflowers.sensor.Arduino;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class ShutdownThread extends Thread {

    @Override
    public void run() {
        Arduino arduino = Wrapper.getArduino();
        if (arduino != null) arduino.close();
        Connector connector = Wrapper.getConnector();
        if (connector.isLogin()) connector.disable();
        log.info("Shutting down Wrapper..");
    }
}
