package de.hackathon.iotflowers.meta;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@AllArgsConstructor
@Data
public class Plant {

    private String uuid, userName, name, roomName;
    private int waterLevel, wetLevel;
    private WaterRequirement waterRequirement;

    public enum WaterRequirement {

        LOW(1000, 2), MIDDLE(800, 5), HIGH(600, 10);

        @Getter
        private int seconds, range;

        WaterRequirement(int seconds, int range) {
            this.seconds = seconds;
            this.range = range;
        }
    }
}
