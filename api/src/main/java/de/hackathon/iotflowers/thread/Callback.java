package de.hackathon.iotflowers.thread;

/**
 * Created by Lennox on 19.02.17.
 */
public interface Callback<T> {

    void done(T args);
}
