package de.hackathon.iotflowers.thread;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Created by Lennox on 26.02.17.
 */
public class AsyncExecutor {

    private static final Executor EXECUTOR = Executors.newCachedThreadPool();

    public static void run(Runnable runnable) {
        EXECUTOR.execute(runnable);
    }
}
