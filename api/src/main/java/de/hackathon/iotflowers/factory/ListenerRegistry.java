package de.hackathon.iotflowers.factory;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import de.hackathon.iotflowers.packet.Packet;
import de.hackathon.iotflowers.packet.PacketHandler;
import de.hackathon.iotflowers.packet.PacketListener;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.log4j.Log4j2;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by Lennox on 29.11.16.
 */
@Log4j2
public class ListenerRegistry {

    private static ListenerRegistry instance;
    private Table<Class<?>, PacketListener, Method> packetListener = HashBasedTable.create();

    public void register(PacketListener listener) {
        for (Method method : listener.getClass().getMethods()) {
            if (method.getAnnotationsByType(PacketHandler.class).length == 0) {
                continue;
            }
            packetListener.put(method.getParameterTypes()[1], listener, method);
        }
    }

    public void callEvent(ChannelHandlerContext channelHandlerContext, Packet packet) {
        if (!packetListener.containsRow(packet.getClass())) {
            return;
        }
        packetListener.row(packet.getClass()).forEach((listener, method) -> {
            try {
                method.invoke(listener, channelHandlerContext, packet);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
               // log.error(e);
            }
        });
    }
}
