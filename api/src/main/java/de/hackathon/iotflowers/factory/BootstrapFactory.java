package de.hackathon.iotflowers.factory;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import io.netty.bootstrap.Bootstrap;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.epoll.Epoll;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollServerSocketChannel;
import io.netty.channel.epoll.EpollSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.util.concurrent.ThreadFactory;

/**
 * Created by Lennox on 19.03.17.
 */
public class BootstrapFactory {

    public static final String PACKET_DECODER = "packet-decoder";
    public static final String PACKET_ENCODER = "packet-encoder";
    public static final String CONNECTION_HANDLER = "connection-handler";
    public static final String FRAME_DECODER = "frame-decoder";
    public static final String FRAME_PREPENDER = "frame-prepender";

    private static final boolean EPOLL = Epoll.isAvailable();

    private ChannelInitializer<Channel> channelChannelInitializer;

    public BootstrapFactory setChannelChannelInitializer(ChannelInitializer<Channel> channelChannelInitializer) {
        this.channelChannelInitializer = channelChannelInitializer;
        return this;
    }

    public Bootstrap buildBootstrap() {
        Bootstrap bootstrap = new Bootstrap()
                .group(newEventLoopGroup(0, new ThreadFactoryBuilder().setNameFormat("client").build()))
                .channel(getChannel())
                .handler(getChannelChannelInitializer()
                );
        Runtime.getRuntime().addShutdownHook(new Thread(() -> bootstrap.group().shutdownGracefully(), "shutDownHook"));
        return bootstrap;
    }

    public ServerBootstrap buildServerBootstrap() {
        ServerBootstrap serverBootstrap = new ServerBootstrap()
                .group(newEventLoopGroup(0, new ThreadFactoryBuilder().setNameFormat("server").build()))
                .channel(getServerChannel())
                .childHandler(getChannelChannelInitializer()
                );
        Runtime.getRuntime().addShutdownHook(new Thread(() -> serverBootstrap.group().shutdownGracefully(), "shutDownHook"));
        return serverBootstrap;
    }

    private ChannelInitializer<Channel> getChannelChannelInitializer() {
        if (this.channelChannelInitializer == null) {
            return new ChannelInitializer<Channel>() {
                @Override
                protected void initChannel(Channel channel) throws Exception {
                    channel.config().setOption(ChannelOption.IP_TOS, 0x18);
                }
            };
        }
        return this.channelChannelInitializer;
    }

    public static EventLoopGroup newEventLoopGroup(int threads, ThreadFactory factory) {
        return EPOLL ? new EpollEventLoopGroup(threads, factory) : new NioEventLoopGroup(threads, factory);
    }

    public static Class<? extends ServerChannel> getServerChannel() {
        return EPOLL ? EpollServerSocketChannel.class : NioServerSocketChannel.class;
    }

    public static Class<? extends Channel> getChannel() {
        return EPOLL ? EpollSocketChannel.class : NioSocketChannel.class;
    }
}
