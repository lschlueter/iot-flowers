package de.hackathon.iotflowers;

/**
 * Created by Lennox on 01.12.16.
 */
public enum ClientType {

    APP,
    FLOWER,
    UNKNOWN
}
