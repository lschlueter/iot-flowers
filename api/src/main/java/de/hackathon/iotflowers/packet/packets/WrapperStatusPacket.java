package de.hackathon.iotflowers.packet.packets;

import de.hackathon.iotflowers.packet.Packet;
import de.hackathon.iotflowers.packet.PacketReader;
import de.hackathon.iotflowers.packet.PacketWriter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.IOException;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class WrapperStatusPacket extends Packet {

    private String connectionName;
    private int waterLevel, wetLevel;
    private boolean motion;

    @Override
    public void read(PacketReader packetReader) throws IOException {
        this.connectionName = packetReader.readString();
        this.waterLevel = packetReader.readInt();
        this.wetLevel = packetReader.readInt();
        this.motion = packetReader.readBoolean();
    }

    @Override
    public void write(PacketWriter packetWriter) throws IOException {
        packetWriter.writeString(this.connectionName);
        packetWriter.writeInt(this.waterLevel);
        packetWriter.writeInt(this.wetLevel);
        packetWriter.writeBoolean(this.motion);
    }
}
