package de.hackathon.iotflowers.packet;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.EmptyByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;

import java.util.List;

/**
 * Created by Lennox on 22.11.16.
 */
@AllArgsConstructor
@Log4j2
public class PacketDecoder extends ByteToMessageDecoder {

    private PacketRegistry packetRegistry;

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        if (!in.isReadable() || in instanceof EmptyByteBuf) {
            return;
        }

        PacketReader packetReader = new PacketReader(in);

        int packetID = in.readInt();
        if (this.packetRegistry.hasPacket(packetID)) {
            Packet packet = this.packetRegistry.createPacket(packetID);
            packet.read(packetReader);
            if (in.readableBytes() != 0) {
                throw new UnsupportedOperationException("Did not read all bytes from packet " + packet.getClass() + " " + packetID);
            }
            out.add(packet);
        } else {
            in.skipBytes(in.readableBytes());
        }
    }
}
