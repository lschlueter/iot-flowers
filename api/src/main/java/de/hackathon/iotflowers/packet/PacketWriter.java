package de.hackathon.iotflowers.packet;

import io.netty.buffer.ByteBuf;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.channels.ScatteringByteChannel;
import java.util.UUID;

/**
 * Created by Lennox on 18.02.17.
 */
public class PacketWriter {

    private final ByteBuf byteBuf;

    public PacketWriter(ByteBuf byteBuf) {
        this.byteBuf = byteBuf;
        this.byteBuf.markWriterIndex();
    }

    public void writeBoolean(boolean b) {
        this.byteBuf.writeBoolean(b);
    }

    public void writeByte(int i) {
        this.byteBuf.writeByte(i);
    }

    public void writeShort(int i) {
        this.byteBuf.writeShort(i);
    }

    public void writeMedium(int i) {
        this.byteBuf.writeMedium(i);
    }

    public void writeInt(int i) {
        this.byteBuf.writeInt(i);
    }

    public void writeLong(long l) {
        this.byteBuf.writeLong(l);
    }

    public void writeChar(int i) {
        this.byteBuf.writeChar(i);
    }

    public void writeFloat(float v) {
        this.byteBuf.writeFloat(v);
    }

    public void writeDouble(double v) {
        this.byteBuf.writeDouble(v);
    }

    public void writeBytes(ByteBuf byteBuf) {
        this.byteBuf.writeBytes(byteBuf);
    }

    public void writeBytes(ByteBuf byteBuf, int i) {
        this.byteBuf.writeBytes(byteBuf, i);
    }

    public void writeBytes(ByteBuf byteBuf, int i, int i2) {
        this.byteBuf.writeBytes(byteBuf, i, i2);
    }

    public void writeBytes(byte[] bytes) {
        this.byteBuf.writeBytes(bytes);
    }

    public void writeBytes(byte[] bytes, int i, int i2) {
        this.byteBuf.writeBytes(bytes, i, i2);
    }

    public void writeBytes(ByteBuffer byteBuffer) {
        this.byteBuf.writeBytes(byteBuffer);
    }

    public int writeBytes(InputStream inputStream, int i) throws IOException {
        return this.byteBuf.writeBytes(inputStream, i);
    }

    public int writeBytes(ScatteringByteChannel scatteringByteChannel, int i) throws IOException {
        return this.byteBuf.writeBytes(scatteringByteChannel, i);
    }

    public void writeUUID(UUID uuid) {
        this.byteBuf.writeLong(uuid.getMostSignificantBits());
        this.byteBuf.writeLong(uuid.getLeastSignificantBits());
    }

    public void writeString(String s) {
        try {
            byte[] string = s.getBytes("UTF-8");
            writeShort(string.length);
            writeBytes(string);
        } catch (UnsupportedEncodingException ignored) {
        }
    }

    public void writeStringArray(String[] array) {
        writeInt(array.length);
        for (String s : array) {
            writeString(s);
        }
    }

    public void writeZero(int i) {
        this.byteBuf.writeZero(i);
    }

    public void resetWriterIndex() {
        this.byteBuf.resetWriterIndex();
    }
}