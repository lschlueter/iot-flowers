package de.hackathon.iotflowers.packet.packets;

import de.hackathon.iotflowers.meta.Plant;
import de.hackathon.iotflowers.packet.Packet;
import de.hackathon.iotflowers.packet.PacketReader;
import de.hackathon.iotflowers.packet.PacketWriter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.IOException;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AppSyncPacket extends Packet {

    private Plant plant;

    @Override
    public void read(PacketReader packetReader) throws IOException {
        this.plant = new Plant(
                packetReader.readString(),
                packetReader.readString(),
                packetReader.readString(),
                packetReader.readString(),
                packetReader.readInt(),
                packetReader.readInt(),
                Plant.WaterRequirement.valueOf(packetReader.readString())
        );
    }

    @Override
    public void write(PacketWriter packetWriter) throws IOException {
        packetWriter.writeString(this.plant.getUuid());
        packetWriter.writeString(this.plant.getUserName());
        packetWriter.writeString(this.plant.getName());
        packetWriter.writeString(this.plant.getRoomName());
        packetWriter.writeInt(this.plant.getWaterLevel());
        packetWriter.writeInt(this.plant.getWetLevel());
        packetWriter.writeString(this.plant.getWaterRequirement().toString());
    }
}
