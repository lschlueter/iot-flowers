package de.hackathon.iotflowers.packet.packets;

import de.hackathon.iotflowers.packet.Packet;
import de.hackathon.iotflowers.packet.PacketReader;
import de.hackathon.iotflowers.packet.PacketWriter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.IOException;

/**
 * Created by Lennox on 29.11.16.
 */
@AllArgsConstructor
@NoArgsConstructor
public class LoginResultPacket extends Packet {


    @Getter private boolean success;

    @Override
    public void read(PacketReader packetReader) throws IOException {
        this.success = packetReader.readBoolean();
    }

    @Override
    public void write(PacketWriter packetWriter) throws IOException {
        packetWriter.writeBoolean(this.success);
    }
}
