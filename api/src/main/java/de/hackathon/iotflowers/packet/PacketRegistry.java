package de.hackathon.iotflowers.packet;

import com.google.common.base.Preconditions;
import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TObjectIntHashMap;
import lombok.extern.log4j.Log4j2;

import java.lang.reflect.Constructor;

/**
 * Created by Lennox on 22.11.16.
 */
@Log4j2
public class PacketRegistry {

    private final static int MAX_PACKET_ID = 0xFF;

    private final TObjectIntMap<Class<? extends Packet>> packetMap = new TObjectIntHashMap<>(MAX_PACKET_ID);
    private final Class<? extends Packet>[] packetClasses = new Class[MAX_PACKET_ID];
    private final Constructor<? extends Packet>[] packetConstructors = new Constructor[MAX_PACKET_ID];

    public boolean hasPacket(int id) {
        return id < MAX_PACKET_ID && packetConstructors[id] != null;
    }

    public final Packet createPacket(int id) {
        if (id > MAX_PACKET_ID) {
            throw new UnsupportedOperationException("Packet with id " + id + " outside of range ");
        }
        if (packetConstructors[id] == null) {
            throw new UnsupportedOperationException("No packet with id " + id);
        }

        try {
            return packetConstructors[id].newInstance();
        } catch (ReflectiveOperationException ex) {
            throw new UnsupportedOperationException("Could not construct packet with id " + id, ex);
        }
    }

    public final void registerPacket(int id, Class<? extends Packet> packetClass) {
        try {
            packetConstructors[id] = packetClass.getDeclaredConstructor();
        } catch (NoSuchMethodException ex) {
            throw new UnsupportedOperationException("No NoArgsConstructor for packet class " + packetClass);
        }
        packetClasses[id] = packetClass;
        packetMap.put(packetClass, id);
    }

    protected final void unregisterPacket(int id) {
        packetMap.remove(packetClasses[id]);
        packetClasses[id] = null;
        packetConstructors[id] = null;
    }

    public final int getId(Class<? extends Packet> packet) {
        Preconditions.checkArgument(packetMap.containsKey(packet), "Cannot get ID for packet " + packet);
        return packetMap.get(packet);
    }
}
