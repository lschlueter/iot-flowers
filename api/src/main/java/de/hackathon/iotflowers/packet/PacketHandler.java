package de.hackathon.iotflowers.packet;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Lennox on 29.11.16.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface PacketHandler {
}
