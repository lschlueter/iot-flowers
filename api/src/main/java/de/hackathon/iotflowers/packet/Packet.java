package de.hackathon.iotflowers.packet;


import lombok.Getter;

import java.io.IOException;

/**
 * Created by Lennox on 22.11.16.
 */
public abstract class Packet {

    @Getter
    protected boolean async;

    public abstract void read(PacketReader packetReader) throws IOException;

    public abstract void write(PacketWriter packetWriter) throws IOException;
}
