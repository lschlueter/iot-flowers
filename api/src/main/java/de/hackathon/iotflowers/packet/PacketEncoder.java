package de.hackathon.iotflowers.packet;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import lombok.AllArgsConstructor;

/**
 * Created by Lennox on 22.11.16.
 */
@AllArgsConstructor
public class PacketEncoder extends MessageToByteEncoder<Packet> {

    private PacketRegistry packetRegistry;

    @Override
    protected void encode(ChannelHandlerContext ctx, Packet msg, ByteBuf out) throws Exception {
        out.writeInt(this.packetRegistry.getId(msg.getClass()));
        msg.write(new PacketWriter(out));
    }
}
