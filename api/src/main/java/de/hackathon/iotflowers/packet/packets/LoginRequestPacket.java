package de.hackathon.iotflowers.packet.packets;

import de.hackathon.iotflowers.ClientType;
import de.hackathon.iotflowers.packet.Packet;
import de.hackathon.iotflowers.packet.PacketReader;
import de.hackathon.iotflowers.packet.PacketWriter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import java.io.IOException;

/**
 * Created by Lennox on 29.11.16.
 */
@AllArgsConstructor
@Getter
@NoArgsConstructor
public class LoginRequestPacket extends Packet {

    private String id;
    private ClientType clientType;

    @Override
    public void read(PacketReader packetReader) throws IOException {
        this.id = packetReader.readString();
        this.clientType = ClientType.valueOf(packetReader.readString());
    }

    @Override
    public void write(PacketWriter packetWriter) throws IOException {
        packetWriter.writeString(this.id);
        packetWriter.writeString(this.clientType.toString());
    }
}
