package de.hackathon.iotflowers.packet;

import io.netty.buffer.ByteBuf;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.channels.GatheringByteChannel;
import java.util.UUID;

/**
 * Created by Lennox on 18.02.17.
 */
public class PacketReader {

    private final ByteBuf byteBuf;

    public PacketReader(ByteBuf byteBuf) {
        this.byteBuf = byteBuf;
        this.byteBuf.markReaderIndex();
    }

    public boolean readBoolean() {
        return this.byteBuf.readBoolean();
    }

    public byte readByte() {
        return this.byteBuf.readByte();
    }

    public short readUnsignedByte() {
        return this.byteBuf.readUnsignedByte();
    }

    public short readShort() {
        return this.byteBuf.readShort();
    }

    public int readUnsignedShort() {
        return this.byteBuf.readUnsignedShort();
    }

    public int readMedium() {
        return this.byteBuf.readMedium();
    }

    public int readUnsignedMedium() {
        return this.byteBuf.readUnsignedMedium();
    }

    public int readInt() {
        return this.byteBuf.readInt();
    }

    public long readUnsignedInt() {
        return this.byteBuf.readUnsignedInt();
    }

    public long readLong() {
        return this.byteBuf.readLong();
    }

    public char readChar() {
        return this.byteBuf.readChar();
    }

    public float readFloat() {
        return this.byteBuf.readFloat();
    }

    public double readDouble() {
        return this.byteBuf.readDouble();
    }

    public void readBytes(int i) {
        this.byteBuf.readBytes(i);
    }

    public void readSlice(int i) {
        this.byteBuf.readSlice(i);
    }

    public void readBytes(ByteBuf byteBuf) {
        this.byteBuf.readBytes(byteBuf);
    }

    public void readBytes(ByteBuf byteBuf, int i) {
        this.byteBuf.readBytes(byteBuf, i);
    }

    public void readBytes(ByteBuf byteBuf, int i, int i2) {
        this.byteBuf.readBytes(byteBuf, i, i2);
    }

    public void readBytes(byte[] bytes) {
        this.byteBuf.readBytes(bytes);
    }

    public void readBytes(byte[] bytes, int i, int i2) {
        this.byteBuf.readBytes(bytes, i, i2);
    }

    public void readBytes(ByteBuffer byteBuffer) {
        this.byteBuf.readBytes(byteBuffer);
    }

    public void readBytes(OutputStream outputStream, int i) throws IOException {
        this.byteBuf.readBytes(outputStream, i);
    }

    public int readBytes(GatheringByteChannel gatheringByteChannel, int i) throws IOException {
        return this.byteBuf.readBytes(gatheringByteChannel, i);
    }

    public UUID readUUID() {
        return new UUID(this.byteBuf.readLong(), this.byteBuf.readLong());
    }


    public String readString() {
        int length = readUnsignedShort();
        byte[] stringData = new byte[length];
        readBytes(stringData);
        try {
            return new String(stringData, "UTF-8");
        } catch (UnsupportedEncodingException ignored) {
        }
        return null; //unreachable statement
    }

    public String[] readStringArray() {
        int length = readInt();
        String[] result = new String[length];
        for (int i = 0; i < result.length; i++) {
            result[i] = readString();
        }
        return result;
    }

    public boolean hasBytes() {
        return byteBuf.readerIndex() < byteBuf.capacity() - 1;
    }

    public ByteBuf getByteBuf() {
        return byteBuf;
    }
}