package de.hackathon.iotflowers.packet.packets;

import de.hackathon.iotflowers.meta.Plant;
import de.hackathon.iotflowers.packet.Packet;
import de.hackathon.iotflowers.packet.PacketReader;
import de.hackathon.iotflowers.packet.PacketWriter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.IOException;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PlantActionPacket extends Packet {

    private Action action;
    private Plant target;

    @Override
    public void read(PacketReader packetReader) throws IOException {
        this.action = Action.valueOf(packetReader.readString());
        this.target = new Plant(
                packetReader.readString(),
                packetReader.readString(),
                packetReader.readString(),
                packetReader.readString(),
                packetReader.readInt(),
                packetReader.readInt(),
                Plant.WaterRequirement.valueOf(packetReader.readString())
        );
    }

    @Override
    public void write(PacketWriter packetWriter) throws IOException {
        packetWriter.writeString(this.action.toString());
        packetWriter.writeString(this.target.getUuid());
        packetWriter.writeString(this.target.getUserName());
        packetWriter.writeString(this.target.getName());
        packetWriter.writeString(this.target.getRoomName());
        packetWriter.writeInt(this.target.getWaterLevel());
        packetWriter.writeInt(this.target.getWetLevel());
        packetWriter.writeString(this.target.getWaterRequirement().toString());
    }

    public enum Action {

        CREATE, DELETE, WATER_ON, WATER_OFF
    }
}
