package de.hackathon.iotflowers;

import de.hackathon.iotflowers.net.Server;
import de.hackathon.iotflowers.thread.AsyncExecutor;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.URL;
import java.util.function.Consumer;

@Log4j2
public class Master {

    @Getter private static Server server;
    @Setter private static String IP;
    @Getter private static PlantStore plantStore;

    public static void main(String[] args) {
        log.info("Initialising master..");

        (plantStore = new PlantStore()).loadPlants();

        fetchLocalAddress(s -> {
            setIP(s);
            AsyncExecutor.run(() -> (server = new Server(IP, 3131)).start());
        });
    }

    private static void fetchLocalAddress(Consumer<String> consumer) {
        AsyncExecutor.run(() -> {
            try {
                URL url = new URL("http://automation.whatismyip.com/n09230945.asp");
                BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
                String ipAddress;
                ipAddress = (in.readLine()).trim();

                if (!(ipAddress.length() > 0)) {
                    try {
                        InetAddress ip = InetAddress.getLocalHost();
                        consumer.accept((ip.getHostAddress()).trim());
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                consumer.accept(ipAddress);
            } catch (Exception e) {
                try {
                    InetAddress ip = InetAddress.getLocalHost();
                    consumer.accept((ip.getHostAddress()).trim());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
    }
}
