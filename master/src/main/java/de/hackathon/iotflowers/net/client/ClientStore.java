package de.hackathon.iotflowers.net.client;

import de.hackathon.iotflowers.ClientType;
import io.netty.channel.Channel;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Lennox on 30.11.16.
 */
public class ClientStore extends LinkedList<ClientData> {

    public void addClient(ClientData clientData) {
        if (isCached(clientData)) {
            removeClient(clientData);
        }
        this.add(clientData);
    }

    public void removeClient(ClientData clientData) {
        for (Iterator<ClientData> iterator = this.listIterator(); iterator.hasNext(); ) {
            ClientData target = iterator.next();
            if (target.getId().equals(clientData.getId())) iterator.remove();
        }
    }

    public void removeClient(String id) {
        ClientData clientData = getClient(id);
        if (clientData != null) removeClient(clientData);
    }

    public ClientData getClient(String id) {
        for (ClientData clientData : this) {
            if (clientData.getId().equals(id)) return clientData;
        }
        return null;
    }

    public ClientData getClient(Channel channel) {
        for (ClientData clientData : this) {
            if (clientData.getChannel().equals(channel)) return clientData;
        }
        return null;
    }

    public boolean isCached(ClientData clientData) {
        for (ClientData target : this) {
            if (target.getId().equals(clientData.getId())) return true;
        }
        return false;
    }

    public boolean isCached(Channel channel) {
        for (ClientData target : this) {
            if (target.getChannel().equals(channel)) return true;
        }
        return false;
    }

    public List<ClientData> getAll(ClientType clientType) {
        return this.stream().filter(clientData -> clientData.getClientType() == clientType).collect(Collectors.toList());
    }

    public List<ClientData> getAll(ClientType... clientType) {
        List<ClientData> result = new ArrayList<>();
        for (ClientData clientData : this) {
            for (ClientType target : clientType) {
                if (clientData.getClientType() == target) result.add(clientData);
            }
        }
        return result;
    }
}
