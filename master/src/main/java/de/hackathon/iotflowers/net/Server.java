package de.hackathon.iotflowers.net;

import de.hackathon.iotflowers.net.client.ClientData;
import de.hackathon.iotflowers.net.client.ClientStore;
import de.hackathon.iotflowers.factory.BootstrapFactory;
import de.hackathon.iotflowers.factory.ListenerRegistry;
import de.hackathon.iotflowers.net.listener.WrapperListener;
import de.hackathon.iotflowers.packet.PacketDecoder;
import de.hackathon.iotflowers.packet.PacketEncoder;
import de.hackathon.iotflowers.packet.PacketRegistry;
import de.hackathon.iotflowers.packet.packets.LoginRequestPacket;
import de.hackathon.iotflowers.packet.packets.LoginResultPacket;
import de.hackathon.iotflowers.packet.packets.PlantActionPacket;
import de.hackathon.iotflowers.packet.packets.WrapperStatusPacket;
import de.hackathon.iotflowers.thread.AsyncExecutor;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

import static de.hackathon.iotflowers.factory.BootstrapFactory.*;

@Log4j2
public class Server {

    private String ip;
    private int port;

    @Getter
    private boolean connectionOpen;
    @Getter
    private static ClientStore clientStore;

    private static PacketRegistry packetRegistry;
    @Getter
    private static ListenerRegistry listenerRegistry;

    public Server(String ip, int port) {
        this.ip = ip;
        this.port = port;

        clientStore = new ClientStore();
        packetRegistry = new PacketRegistry();
        listenerRegistry = new ListenerRegistry();
    }

    public void start() {

        // Register Packets/Listeners
        registerPackets();
        registerListeners();

        ServerBootstrap serverBootstrap = new BootstrapFactory().setChannelChannelInitializer(new ChannelInitializer<Channel>() {

            @Override
            protected void initChannel(Channel channel) throws Exception {
                ConnectionHandler connectionHandler = new ConnectionHandler();
                channel.pipeline().addLast(FRAME_DECODER, new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 4, 0, 4));
                channel.pipeline().addLast(PACKET_DECODER, new PacketDecoder(packetRegistry));
                channel.pipeline().addLast(FRAME_PREPENDER, new LengthFieldPrepender(4));
                channel.pipeline().addLast(PACKET_ENCODER, new PacketEncoder(packetRegistry));
                channel.pipeline().addLast(CONNECTION_HANDLER, connectionHandler);
            }
        }).buildServerBootstrap();
        try {
            ChannelFuture channelFuture = serverBootstrap.bind(this.ip, this.port).sync();
            log.info("Server Listening on " + this.ip + ":" + this.port);

            this.connectionOpen = true;

            channelFuture.channel().closeFuture().addListener(f -> log.info("Server closed!")).sync();
        } catch (InterruptedException e) {
            log.error("Could not start server! " + e.getMessage());
            this.connectionOpen = false;
        }
    }

    public void stop() {
        AsyncExecutor.run(() -> {
            log.info("Closing all connections...");
            if (clientStore.size() > 0) for (ClientData clientData : clientStore) {
                clientData.getChannel().disconnect();
            }

        });
    }

    private static void registerPackets() {
        packetRegistry.registerPacket(0, LoginRequestPacket.class);
        packetRegistry.registerPacket(1, LoginResultPacket.class);
        packetRegistry.registerPacket(2, WrapperStatusPacket.class);
        packetRegistry.registerPacket(3, WrapperStatusPacket.class);
        packetRegistry.registerPacket(4, PlantActionPacket.class);
    }

    private static void registerListeners() {
        listenerRegistry.register(new WrapperListener());
    }
}
