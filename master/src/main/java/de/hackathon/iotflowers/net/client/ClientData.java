package de.hackathon.iotflowers.net.client;

import de.hackathon.iotflowers.net.ConnectionHandler;
import de.hackathon.iotflowers.ClientType;
import de.hackathon.iotflowers.packet.Packet;
import io.netty.channel.Channel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Lennox on 01.12.16.
 */
@Getter
@AllArgsConstructor
public class ClientData {

    private String id;
    private ClientType clientType;
    private ConnectionHandler connectionHandler;
    private Channel channel;

    @Setter
    private boolean accessible;

    public void sendPacket(Packet packet) {
        this.connectionHandler.dispatchPacket(packet);
    }
}
