package de.hackathon.iotflowers.net;

import de.hackathon.iotflowers.ClientType;
import de.hackathon.iotflowers.factory.ListenerRegistry;
import de.hackathon.iotflowers.net.client.ClientData;
import de.hackathon.iotflowers.net.client.ClientStore;
import de.hackathon.iotflowers.packet.Packet;
import de.hackathon.iotflowers.packet.packets.LoginRequestPacket;
import de.hackathon.iotflowers.packet.packets.LoginResultPacket;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

/**
 * Created by Lennox on 17.03.17.
 */
@Log4j2
public class ConnectionHandler extends SimpleChannelInboundHandler<Packet> {

    private ListenerRegistry listenerRegistry;

    @Getter
    private ChannelHandlerContext ctx;

    public ConnectionHandler() {
        this.listenerRegistry = Server.getListenerRegistry();
    }

    public Channel getChannel() {
        return this.ctx == null ? null : this.ctx.channel();
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        this.ctx = ctx;
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        this.ctx = null;

        // Remove Client
        ClientStore clientStore = Server.getClientStore();
        ClientData clientData = clientStore.getClient(ctx.channel());

        // If cache exists
        if (clientData != null) {
            clientStore.removeClient(clientData);
        } else {
            // else create "null" instance
            clientData = new ClientData("unknown", ClientType.UNKNOWN, null, null, true);
        }
        log.info("Logout for: " + clientData.getId() + "/" + clientData.getChannel().remoteAddress() + " (" + clientData.getClientType().toString() + ")");
    }

    public boolean isChannelOpen() {
        Channel channel = getChannel();
        return channel != null && channel.isOpen();
    }

    public void dispatchPacket(Packet packet) {
        if (!isChannelOpen()) return;
        Channel channel = getChannel();

        if (!packet.isAsync()) {
            // Sync sending
            channel.writeAndFlush(packet).addListener(ChannelFutureListener.FIRE_EXCEPTION_ON_FAILURE);
            return;
        }
        // Async sending
        channel.eventLoop().execute(() -> channel.writeAndFlush(packet).addListener(ChannelFutureListener.FIRE_EXCEPTION_ON_FAILURE));
    }

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, Packet packet) throws Exception {
        if (!getChannel().isOpen()) return;

        // Handle Login
        if (packet instanceof LoginRequestPacket) {
            handleLogin((LoginRequestPacket) packet);
            return;
        }
        this.listenerRegistry.callEvent(channelHandlerContext, packet);
    }

    private void handleLogin(LoginRequestPacket loginRequestPacket) {
        String id = loginRequestPacket.getId();
        ClientStore clientStore = Server.getClientStore();
        ClientData clientData = clientStore.getClient(id);

        // Failed if already exists
        if (clientData == null) {

            // Register
            clientData = new ClientData(loginRequestPacket.getId(), loginRequestPacket.getClientType(), this, this.getChannel(), true);
            clientStore.addClient(clientData);

            // Send result
            clientData.sendPacket(new LoginResultPacket(true));
        }
        log.info("Login for " + loginRequestPacket.getId() + "/" + clientData.getChannel().remoteAddress() + " (" + loginRequestPacket.getClientType().toString() + ")");
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        if (getChannel().isActive()) {
            ctx.close();
        }
        throw new Exception(cause);
    }
}