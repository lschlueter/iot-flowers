package de.hackathon.iotflowers.net.listener;

import de.hackathon.iotflowers.ClientType;
import de.hackathon.iotflowers.Master;
import de.hackathon.iotflowers.PlantStore;
import de.hackathon.iotflowers.meta.Plant;
import de.hackathon.iotflowers.net.Server;
import de.hackathon.iotflowers.net.client.ClientData;
import de.hackathon.iotflowers.net.client.ClientStore;
import de.hackathon.iotflowers.packet.PacketHandler;
import de.hackathon.iotflowers.packet.PacketListener;
import de.hackathon.iotflowers.packet.packets.AppSyncPacket;
import de.hackathon.iotflowers.packet.packets.PlantActionPacket;
import de.hackathon.iotflowers.packet.packets.WrapperStatusPacket;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class WrapperListener extends PacketListener {

    @PacketHandler
    public void onStatus(ChannelHandlerContext channelHandlerContext, WrapperStatusPacket wrapperStatusPacket) {
        ClientStore clientStore = Server.getClientStore();
        ClientData clientData = clientStore.getClient(wrapperStatusPacket.getConnectionName());

        if (clientData == null) {
            log.error("Client with id '" + wrapperStatusPacket.getConnectionName() + "' does not exists! closing..");
            channelHandlerContext.channel().close();
            return;
        }
        PlantStore plantStore = Master.getPlantStore();
        Plant plant = plantStore.getPlantByUser("Consuela");

        // sync
        for (ClientData app : clientStore) {
            if (app.getClientType() != ClientType.APP) continue;
            app.sendPacket(new AppSyncPacket(plant));
        }

        // check
        if (plant.getWetLevel() >= plant.getWaterRequirement().getRange()) {
            for (ClientData target : clientStore) {
                if (target.getClientType() == ClientType.APP) continue;
                target.sendPacket(new PlantActionPacket(PlantActionPacket.Action.WATER_ON, plant));
            }
        }

        if (plant.getWaterLevel() <= 100) {
            // NOTIFICATION
        }

        /*log.info("Wrapper: "
                + wrapperStatusPacket.getConnectionName()
                + " / Water-Level: "
                + wrapperStatusPacket.getWaterLevel()
                + " / Wet-Level: "
                + wrapperStatusPacket.getWetLevel());*/
    }

    @PacketHandler
    public void onAction(ChannelHandlerContext channelHandlerContext, PlantActionPacket plantActionPacket) {
        PlantStore plantStore = Master.getPlantStore();
        switch (plantActionPacket.getAction()) {
            case CREATE:
                plantStore.createPlant(plantActionPacket.getTarget());
                break;
            case DELETE:
                plantStore.deletePlant(plantActionPacket.getTarget());
                break;
        }
    }
}
