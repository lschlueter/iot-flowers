package de.hackathon.iotflowers;

import de.hackathon.iotflowers.net.Server;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class ShutdownThread extends Thread {

    @Override
    public void run() {
        Server server = Master.getServer();
        if (server.isConnectionOpen()) server.stop();
        log.info("Shutting down Wrapper..");
    }
}
