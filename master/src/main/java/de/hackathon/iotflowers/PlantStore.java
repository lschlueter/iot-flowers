package de.hackathon.iotflowers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.hackathon.iotflowers.meta.Plant;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lennox on 19.03.17.
 */
@Log4j2
public class PlantStore {

    @Getter
    private final List<Plant> PLANTS = new ArrayList<>();
    private Gson gson;

    public PlantStore() {
        this.gson = new GsonBuilder().setPrettyPrinting().create();
    }

    public void loadPlants() {
        File temp = Paths.get("files/plants").toFile();
        File[] files = temp.listFiles();

        if (!temp.exists() || files == null) {
            log.error("Could not load plants! Please restart..");
            return;
        }

        for (File file : files) {
            try (FileReader fileReader = new FileReader(file)) {
                Plant plant = gson.fromJson(fileReader, Plant.class);
                this.PLANTS.add(plant);
                log.info("Loaded plant '" + plant.getName() + "'!");
            } catch (IOException e) {
                log.fatal(e);
            }
        }
    }

    public void createPlant(Plant plant) {
        try (FileWriter fileWriter = new FileWriter(new File("files/plants/" + plant.getName() + ".json"))) {
            gson.toJson(plant, fileWriter);
            this.PLANTS.add(plant);
        } catch (Exception e) {
            log.fatal("Could not create plant:", e);
        }
    }

    public void deletePlant(Plant plant) {
        File temp = Paths.get("files/plants/" + plant.getName() + ".json").toFile();
        if (!temp.exists() || !temp.delete()) {
            log.error("Could not delete plant '" + plant.getName() + "'!");
            return;
        }
        this.PLANTS.removeIf(target -> target.getName().equals(plant.getName()));
    }

    public Plant getPlantByUser(String username) {
        for (Plant plant : PLANTS) if (plant.getUserName().equals(username)) return plant;
        return null;
    }
}